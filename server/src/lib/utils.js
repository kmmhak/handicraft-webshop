import crypto from "crypto";
import jwt from "jsonwebtoken";
import { config } from "dotenv";

if (process.env.NODE_ENV === "dev" || process.env.NODE_ENV === undefined) {
  console.log(process.env.NODE_ENV);
  config();
}

export const genJwt = (user) => {
  const newJwt = jwt.sign(
    {
      sub: user.rows[0].id,
      iat: new Date().getTime(),
      exp: new Date().setDate(new Date().getDate() + 1),
    },
    process.env.SECRET
  );
  return { token: `Bearer ${newJwt}` };
};

export const genSaltHash = (password) => {
  const salt = crypto.randomBytes(32).toString("hex");
  const hash = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");

  return {
    salt,
    hash,
  };
};

export const validPassword = (password, hash, salt) => {
  const hashVerify = crypto
    .pbkdf2Sync(password, salt, 10000, 64, "sha512")
    .toString("hex");
  return hash === hashVerify;
};

export const isAdmin = (role) => {
  if (role !== "admin") {
    return false;
  }
  return true;
};

export function validateEmail(email) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

export const categories = [
  { id: 1, name: "fabrics" },
  { id: 2, name: "yarns" },
  { id: 3, name: "bobbin lace" },
  { id: 4, name: "hobby crafts" },
];

export const subcategories = [
  { id: 1, name: "stretch" },
  { id: 2, name: "college" },
  { id: 3, name: "wool" },
  { id: 4, name: "cotton" },
  { id: 5, name: "bobbin pillows" },
  { id: 6, name: "bobbins" },
  { id: 5, name: "bobbin pillows" },
  { id: 6, name: "bobbins" },
  { id: 7, name: "card supplies" },
  { id: 8, name: "decorations" },
];

export const categoryToNumber = (categoryName) => {
  const category = categories.find((c) => c.name === categoryName);
  return category.id;
};

export const subcategoryToNumber = (subcategoryName) => {
  const subcategory = subcategories.find((c) => c.name === subcategoryName);
  return subcategory.id;
};
