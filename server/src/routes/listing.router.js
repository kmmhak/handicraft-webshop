import { Router } from "express";
import passport from "passport";
import * as controller from "../controllers/listing.controller.js";
import {
  passportJwt,
  jwtStrategy,
} from "../middleware/passport-jwt.middleware.js";

const listingRouter = Router();
passport.use(jwtStrategy);

listingRouter.get("/newest", controller.getNewest);
listingRouter.get("/", controller.getAll);
listingRouter.get("/:id", controller.getById);
listingRouter.get("/categories/:category", controller.getByCategory);
listingRouter.get("/subcategories/:subcategory", controller.getBySubcategory);
listingRouter.post("/", passportJwt(), controller.addListing);
listingRouter.get("/user/:id", controller.getByUserId);
listingRouter.delete("/:id", passportJwt(), controller.deleteById);
listingRouter.put("/edit/:id", passportJwt(), controller.editListing);

export default listingRouter;
