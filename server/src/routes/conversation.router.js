import { Router } from "express";
import passport from "passport";
import * as controller from "../controllers/conversation.controller.js";
import {
  passportJwt,
  jwtStrategy,
} from "../middleware/passport-jwt.middleware.js";

const conversationRouter = Router();
passport.use(jwtStrategy);

conversationRouter.get("/", passportJwt(), controller.usersConversations);
conversationRouter.get("/:id", passportJwt(), controller.usersMessages);
conversationRouter.post("/", passportJwt(), controller.sendMessages);
conversationRouter.post(
  "/listing/:id",
  passportJwt(),
  controller.sendNewMessage
);

export default conversationRouter;
