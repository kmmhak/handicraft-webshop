import * as Conversation from "../services/conversation.service.js";

export const usersConversations = async (req, res) => {
  const userId = req.user.id;
  Conversation.usersConversations(res, userId);
};

export const usersMessages = async (req, res) => {
  const userId = Number(req.user.id);
  const id = req.params.id;
  Conversation.usersMessages(res, id, userId);
};

export const sendMessages = async (req, res) => {
  const userId = req.user.id;
  const { message, fk_conversations_id, fk_recipient_id } = req.body;
  Conversation.sendMessages(
    res,
    userId,
    message,
    fk_conversations_id,
    fk_recipient_id
  );
};

export const sendNewMessage = (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  const { fk_listings_id, fk_recipient_id, message } = req.body;
  Conversation.sendNewMessage(
    res,
    id,
    userId,
    fk_listings_id,
    fk_recipient_id,
    message
  );
};
