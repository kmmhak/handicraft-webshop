import * as User from "../services/user.service.js";

export const getById = async (req, res) => {
  const id = Number(req.params.id);
  User.getById(res, id);
};

export const getAll = async (req, res) => {
  User.getAll(res);
};

export const changePassword = async (req, res) => {
  const { newPassword, newPassword2, oldPassword } = req.body;
  const userId = req.user.id;
  User.changePassword(res, userId, newPassword, newPassword2, oldPassword);
};

export const updateInfo = async (req, res) => {
  const { email, password } = req.body;
  const userId = req.user.id;
  User.updateInfo(res, userId, email, password);
};

export const deleteUser = async (req, res) => {
  const id = Number(req.params.id);
  const userId = Number(req.user.id);
  User.deleteUser(res, id, userId);
};

export const register = async (req, res) => {
  const { username, email, password } = req.body;
  User.register(res, username, email, password);
};

export const login = async (req, res) => {
  const { email, password } = req.body;
  User.login(res, email, password);
};

export const authenticate = async (req, res) => {
  const userId = req.user.id;
  User.authenticate(res, userId);
};

export const changeRole = async (req, res) => {
  const userId = req.user.id;
  const id = req.params.id;
  const { role } = req.body;
  User.changeRole(res, userId, id, role);
};
