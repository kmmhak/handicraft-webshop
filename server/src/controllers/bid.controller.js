import * as Bid from "../services/bid.service.js";

export const getById = async (req, res) => {
  const id = Number(req.params.id);
  Bid.getById(res, id);
};

export const makeBid = async (req, res) => {
  const id = Number(req.params.id);
  const userId = req.user.id;
  Bid.makeBid(res, id, userId);
};

export const bidHistory = async (req, res) => {
  const id = Number(req.params.id);
  Bid.bidHistory(res, id);
};

export const getListingBids = async (req, res) => {
  const id = Number(req.params.id);
  Bid.getListingBids(res, id);
};
