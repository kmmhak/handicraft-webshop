import * as Listing from "../services/listing.service.js";

export const getAll = async (req, res) => {
  Listing.getAll(res);
};

export const getNewest = async (req, res) => {
  Listing.getNewest(res);
};

export const getById = async (req, res) => {
  const id = Number(req.params.id);
  Listing.getById(res, id);
};

export const getByUserId = async (req, res) => {
  const id = req.params.id;
  Listing.getByUserId(res, id);
};

export const addListing = async (req, res) => {
  const userId = req.user.id;
  const {
    title,
    brand,
    img,
    length,
    unit,
    color,
    description,
    price,
    category,
    subcategory,
    longitude,
    latitude,
  } = req.body;

  Listing.addListing(
    res,
    userId,
    title,
    brand,
    img,
    length,
    unit,
    color,
    description,
    price,
    category,
    subcategory,
    longitude,
    latitude
  );
};

export const deleteById = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  Listing.deleteById(res, id, userId);
};

export const editListing = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  const {
    title,
    brand,
    img,
    length,
    unit,
    color,
    description,
    price,
    category,
    subcategory,
    latitude,
    longitude,
  } = req.body;

  Listing.editListing(
    res,
    id,
    userId,
    title,
    brand,
    img,
    length,
    unit,
    color,
    description,
    price,
    category,
    subcategory,
    latitude,
    longitude
  );
};

export const getByCategory = async (req, res) => {
  const { category } = req.params;
  Listing.getByCategory(res, category);
};

export const getBySubcategory = async (req, res) => {
  const { subcategory } = req.params;
  Listing.getBySubcategory(res, subcategory);
};
