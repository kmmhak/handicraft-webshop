import pool from "../db/dbConfig.js";
import { categoryToNumber, subcategoryToNumber } from "../lib/utils.js";

export const getAll = async (res) => {
  try {
    const listings = await pool.query("SELECT * FROM listings");
    res.status(200).json({ listings: listings.rows });
  } catch (error) {
    res.status(400).json({ message: "Error getting listings" });
  }
};

export const getNewest = async (res) => {
  try {
    const newest = await pool.query(
      `SELECT 
          listings.id, 
          title, 
          img, 
          price, 
          categories.name AS category, 
          subcategories.name AS subcategory, 
          users.username, 
          added 
          longitude,
          latitude
          FROM listings 
          LEFT JOIN categories ON listings.fk_categories_id=categories.id
          LEFT JOIN subcategories ON listings.fk_subcategories_id=subcategories.id
          LEFT JOIN users ON listings.fk_users_id=users.id
          ORDER BY added DESC 
          LIMIT 5;`
    );

    res.status(200).json({ newest: newest.rows });
  } catch (error) {
    res.status(400).json({ message: "Error getting newest listings" });
  }
};

export const getById = async (res, id) => {
  try {
    const listings = await pool.query(
      `SELECT 
      listings.id, 
      title, 
      brand, 
      img, 
      length, 
      unit, 
      color, 
      description, 
      price, 
      categories.name AS category, 
      categories.id AS category_id,
      subcategories.name AS subcategory, 
      subcategories.id AS subcategory_id,
      users.username, 
      users.id AS user_id,
      added,
      longitude,
      latitude 
      FROM listings 
      LEFT JOIN categories ON listings.fk_categories_id=categories.id
      LEFT JOIN subcategories ON listings.fk_subcategories_id=subcategories.id
      LEFT JOIN users ON listings.fk_users_id=users.id
	    WHERE listings.id = $1`,
      [id]
    );

    if (listings.rows.length > 0) {
      res.status(200).json({ listings: listings.rows });
    } else {
      res.status(404).json({ message: "No listing with given id" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error getting listing" });
  }
};

export const getByUserId = async (res, id) => {
  try {
    if (isNaN(id)) {
      res.status(400).json({ message: "id needs to be a number" });
    } else {
      const foundUser = await pool.query(`SELECT * FROM users where id= $1`, [
        id,
      ]);

      if (foundUser.rows.length == 0) {
        res.status(404).json({ message: "User not found" });
      } else {
        const userListings = await pool.query(
          `SELECT * FROM listings WHERE fk_users_id = $1`,
          [id]
        );

        if (userListings.rows.length > 0) {
          res.status(200).json({ userListings: userListings.rows });
        } else {
          res.status(200).json({ message: "User has no listings" });
        }
      }
    }
  } catch (error) {
    res.status(400).json({ message: "Error getting listings" });
  }
};

export const addListing = async (
  res,
  userId,
  title,
  brand,
  img,
  length,
  unit,
  color,
  description,
  price,
  category,
  subcategory,
  longitude,
  latitude
) => {
  try {
    if (
      !title ||
      !description ||
      !length ||
      !price ||
      !category ||
      !subcategory
    ) {
      res.status(400).json({ message: "Please give all required fields" });
    }

    if (
      !Number.isInteger(subcategory) ||
      !Number.isInteger(category) ||
      !Number.isInteger(length) ||
      !Number.isInteger(price)
    ) {
      res.status(400).json({
        message:
          "price, length, user, category and subcategory must be integers",
      });
    } else {
      const newListing = await pool.query(
        `INSERT INTO listings 
      (title,
      brand,
      img,
      length,
      unit,
      color,
      description,
      price,
      longitude,
      latitude,
      fk_categories_id,
      fk_subcategories_id,
      fk_users_id) 
      VALUES
      ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)`,
        [
          title,
          brand,
          img,
          length,
          unit,
          color,
          description,
          price,
          longitude,
          latitude,
          category,
          subcategory,
          userId,
        ]
      );

      res.status(200).json({ message: "New listing added" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error adding a new listing" });
  }
};

export const deleteById = async (res, id, userId) => {
  try {
    if (isNaN(id)) {
      res.status(400).json({ message: "id needs to be a number" });
    } else {
      const foundListing = await pool.query(
        `SELECT * FROM listings where id = $1`,
        [id]
      );

      if (foundListing.rows.length == 0) {
        res.status(404).json({ message: "Listing not found" });
      } else if (foundListing.rows[0].fk_users_id != userId) {
        res
          .status(403)
          .json({ message: "You can only delete your own listings" });
      } else {
        await pool.query(`DELETE FROM listings WHERE id = $1`, [id]);

        res.status(200).json({ message: "listing deleted" });
      }
    }
  } catch (error) {
    res.status(400).json({ message: "Error deleting the listing" });
  }
};

export const editListing = async (
  res,
  id,
  userId,
  title,
  brand,
  img,
  length,
  unit,
  color,
  description,
  price,
  category,
  subcategory,
  latitude,
  longitude
) => {
  try {
    if (
      !title ||
      !description ||
      !length ||
      !price ||
      !category ||
      !subcategory
    ) {
      res.status(400).json({ message: "Please give all required fields" });
    }

    if (
      !Number.isInteger(subcategory) ||
      !Number.isInteger(category) ||
      !Number.isInteger(length) ||
      !Number.isInteger(price)
    ) {
      res.status(400).json({
        message:
          "price, length, user, category and subcategory must be integers",
      });
    } else {
      const editedListing = await pool.query(
        `UPDATE listings 
      SET title = $1,
      brand = $2,
      img = $3,
      length = $4,
      unit = $5,
      color = $6,
      description = $7,
      price = $8,
      latitude = $9,
      longitude = $10,
      fk_categories_id = $11,
      fk_subcategories_id = $12,
      fk_users_id = $13
      WHERE id = $14 
      `,
        [
          title,
          brand,
          img,
          length,
          unit,
          color,
          description,
          price,
          latitude,
          longitude,
          category,
          subcategory,
          userId,
          id,
        ]
      );

      res.status(200).json({ message: "Listing information edited" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error editing listing information" });
  }
};

export const getByCategory = async (res, category) => {
  try {
    const convertedCategory = categoryToNumber(category);

    const listings = await pool.query(
      `SELECT  
      listings.id, 
      title, 
      img, 
      price, 
      categories.name AS category, 
      subcategories.name AS subcategory, 
      users.username, 
      added 
      longitude,
      latitude
      FROM listings 
      LEFT JOIN categories ON listings.fk_categories_id=categories.id
      LEFT JOIN subcategories ON listings.fk_subcategories_id=subcategories.id
      LEFT JOIN users ON listings.fk_users_id=users.id
      WHERE fk_categories_id = $1`,
      [convertedCategory]
    );

    res.status(200).json({ listings: listings.rows });
  } catch (error) {
    res
      .status(400)
      .json({ message: `Error getting listings in category ${category}` });
  }
};

export const getBySubcategory = async (res, subcategory) => {
  try {
    const convertedSubategory = subcategoryToNumber(subcategory);

    const listings = await pool.query(
      `SELECT
      listings.id, 
      title, 
      img, 
      price, 
      categories.name AS category, 
      subcategories.name AS subcategory, 
      users.username, 
      added 
      longitude,
      latitude
      FROM listings 
      LEFT JOIN categories ON listings.fk_categories_id=categories.id
      LEFT JOIN subcategories ON listings.fk_subcategories_id=subcategories.id
      LEFT JOIN users ON listings.fk_users_id=users.id
      WHERE fk_subcategories_id = $1`,
      [convertedSubategory]
    );
    res.status(200).json({ listings: listings.rows });
  } catch (error) {
    res.status(400).json({ message: "Error getting listings" });
  }
};
