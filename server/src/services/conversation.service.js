import pool from "../db/dbConfig.js";

export const addMessage = async (
  message,
  fk_conversations_id,
  fk_sender_id,
  fk_recipient_id
) => {
  const newMessage = await pool.query(
    `INSERT INTO messages 
      (message, fk_conversations_id, fk_sender_id, fk_recipient_id) 
      VALUES ($1, $2, $3, $4)
      RETURNING *`,
    [message, fk_conversations_id, fk_sender_id, fk_recipient_id]
  );
  return newMessage;
};

export const updateConversation = async (updated_at, fk_conversations_id) => {
  const updatedConversation = await pool.query(
    `UPDATE conversations SET updated_at = $1 WHERE id = $2`,
    [updated_at, fk_conversations_id]
  );
  return updatedConversation;
};

export const usersConversations = async (res, userId) => {
  try {
    const conversations = await pool.query(
      `SELECT 
      conversations.id AS conversations_id, 
      listings.title, 
      fk_listings_id, 
      fk_sender_id, 
      sender.username AS sender_username, 
      fk_recipient_id, 
      recipient.username AS recipient_username, 
      updated_at,  
      listings.img,
      (SELECT message FROM messages WHERE fk_conversations_id=conversations.id ORDER BY sent_at DESC LIMIT 1) 
      FROM conversations
      LEFT JOIN listings ON listings.id = fk_listings_id
      LEFT JOIN users sender ON sender.id=conversations.fk_sender_id
	    LEFT JOIN users recipient ON recipient.id=conversations.fk_recipient_id
      WHERE fk_sender_id =$1 OR fk_recipient_id= $1 
      ORDER BY updated_at DESC`,
      [userId]
    );

    res.status(200).json({ message: conversations.rows });
  } catch (error) {
    res.status(400).json({ message: "Error getting conversations" });
  }
};

export const usersMessages = async (res, id, userId) => {
  try {
    const messages = await pool.query(
      `SELECT 
      messages.id, 
      message, 
      sent_at, 
      fk_conversations_id, 
      messages.fk_sender_id, 
      messages.fk_recipient_id, 
      sender.username as sender_username, 
      recipient.username as recipient_username
      FROM messages 
      LEFT JOIN conversations ON conversations.id = messages.fk_conversations_id
      LEFT JOIN users sender ON sender.id = messages.fk_sender_id
      LEFT JOIN users recipient ON recipient.id = messages.fk_recipient_id
      WHERE fk_conversations_id=$1`,
      [id]
    );

    if (
      userId === Number(messages.rows[0].fk_sender_id) ||
      userId === Number(messages.rows[0].fk_recipient_id)
    ) {
      res.status(200).json({ message: messages.rows });
    } else {
      res.status(401).json({ message: "Unauthorized" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error getting messages" });
  }
};

export const sendMessages = async (
  res,
  userId,
  message,
  fk_conversations_id,
  fk_recipient_id
) => {
  try {
    if (message && fk_conversations_id && !isNaN(fk_conversations_id)) {
      const newMessage = await addMessage(
        message,
        fk_conversations_id,
        userId,
        fk_recipient_id
      );
      const updatedConversation = await updateConversation(
        newMessage.rows[0].sent_at,
        fk_conversations_id
      );

      res.status(200).json({ message: "Message sent" });
    } else {
      res
        .status(400)
        .json({ message: "Please insert all fields with appropriate values" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error sending message" });
  }
};

export const sendNewMessage = async (
  res,
  id,
  userId,
  fk_listings_id,
  fk_recipient_id,
  message
) => {
  try {
    const foundConversation = await pool.query(
      `SELECT * 
        FROM conversations 
        WHERE fk_listings_id = $1 AND fk_sender_id = $2  AND fk_recipient_id =$3`,
      [fk_listings_id, userId, fk_recipient_id]
    );

    if (foundConversation.rowCount === 0) {
      const newConversation = await pool.query(
        `INSERT INTO conversations 
          (fk_listings_id, fk_sender_id, fk_recipient_id)
          VALUES ($1, $2, $3)
          RETURNING id`,
        [fk_listings_id, userId, fk_recipient_id]
      );

      const newMessage = addMessage(
        message,
        newConversation.rows[0].id,
        userId,
        fk_recipient_id
      );
      res.status(200).json({ message: "Message sent" });
    } else {
      const newMessage = addMessage(
        message,
        foundConversation.rows[0].id,
        userId,
        fk_recipient_id
      );
      res.status(200).json({ message: "Message sent" });
    }
  } catch (error) {
    res.status(400).json({ message: "Error sending message" });
  }
};
