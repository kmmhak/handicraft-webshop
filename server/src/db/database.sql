CREATE TYPE roleEnum AS enum ('regular', 'admin', 'master');

CREATE TABLE users(
id BIGSERIAL PRIMARY KEY NOT NULL,
username VARCHAR(50) NOT NULL,
email VARCHAR(50) UNIQUE NOT NULL,
salt VARCHAR NOT NULL,
hash VARCHAR NOT NULL,
role roleEnum DEFAULT 'regular'
);

CREATE TABLE categories (
id SERIAL PRIMARY KEY NOT NULL,
name VARCHAR(50) NOT NULL
);

CREATE TABLE subcategories (
id SERIAL PRIMARY KEY NOT NULL,
name VARCHAR(50) NOT NULL
);

CREATE TYPE unitEnum AS enum ('pcs', 'cm', 'g');

CREATE TABLE listings(
id BIGSERIAL PRIMARY KEY NOT NULL,
title VARCHAR(50) NOT NULL,
brand VARCHAR(50),
img VARCHAR,
length INT NOT NULL,
unit unitEnum,
color VARCHAR(30),
description VARCHAR(500) NOT NULL,
price INT NOT NULL,
added TIMESTAMPTZ NOT NULL DEFAULT NOW(),
longitude DECIMAL,
latitude DECIMAL,
fk_categories_id INT not NULL,
fk_subcategories_id INT not NULL,
fk_users_id BIGINT not NULL,
CONSTRAINT fk_categories_id
	FOREIGN KEY(fk_categories_id)
		REFERENCES categories(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_subcategories_id
	FOREIGN KEY(fk_subcategories_id)
		REFERENCES subcategories(id)
		ON DELETE CASCADE 
		ON UPDATE CASCADE,
CONSTRAINT fk_users_id
	FOREIGN KEY(fk_users_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE bids (
id BIGSERIAL PRIMARY KEY NOT NULL,
fk_listings_id BIGINT,
fk_users_id BIGINT,
bid_time TIMESTAMPTZ DEFAULT NOW(),
CONSTRAINT fk_listings_id
	FOREIGN KEY(fk_listings_id)
		REFERENCES listings(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_users_id
	FOREIGN KEY(fk_users_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE conversations(
id BIGSERIAL PRIMARY KEY NOT NULL,
fk_listings_id BIGINT NOT NULL,
fk_sender_id BIGINT NOT NULL,
fk_recipient_id BIGINT NOT NULL,
updated_at TIMESTAMPTZ DEFAULT NOW(),
CONSTRAINT fk_listings_id
	FOREIGN KEY(fk_listings_id)
		REFERENCES listings(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_sender_id
	FOREIGN KEY(fk_sender_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_recipient_id
	FOREIGN KEY(fk_recipient_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);


CREATE TABLE messages (
id BIGSERIAL PRIMARY KEY NOT NULL,
message VARCHAR(500) NOT NULL,
sent_at TIMESTAMPTZ DEFAULT NOW(),
fk_conversations_id BIGINT NOT NULL,
fk_sender_id BIGINT NOT NULL,
fk_recipient_id BIGINT NOT NULL,
CONSTRAINT fk_converstaions_id
	FOREIGN KEY(fk_conversations_id)
		REFERENCES conversations(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_sender_id
	FOREIGN KEY(fk_sender_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
CONSTRAINT fk_recipient_id
	FOREIGN KEY(fk_recipient_id)
		REFERENCES users(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

INSERT INTO users(id, username, email, salt, hash)
VALUES ('0', 'Anonymous', 'anonymous@example.com', 'salt', 'hash');

CREATE RULE "protect_anonymous_delete" AS
ON DELETE to users
WHERE OLD.ID = '0'
DO INSTEAD NOTHING;

CREATE RULE "protect_anonymous_update" AS
ON UPDATE TO users
WHERE OLD.ID = '0'
DO INSTEAD NOTHING;

INSERT INTO users (username, email, salt, hash)
VALUES
('Minnie', 'minnie@example.com', 'b938f24d56f6ce1e511b6701746798964ae551f9015957e27651a67ebd239ccb', 'f29722bfd42690d9d14d316c3df6ed5678d2f2121c37ee27f8326cfe270bbe3a96892d6809e5b71e46b203e7e3a74913eeae64ef784efcc3a267889c28aa2b7c'),
('Pluto', 'pluto@example.com', 'e36220b6450c6fff2171f68c6b71ef036f54787fe9c8560f9dbe4b97dbe58047', '69835cfbd3661f2e3c5380e4e713a5823ded52a82dda41767d96bb038379af4ed70127006dd855e94a86c97ac384e9b2b99e586b9122186f2686f001080c6f1c'),
('Goofy', 'goofy@example.com', 'b938f24d56f6ce1e511b6701746798964ae551f9015957e27651a67ebd239ccb', 'f29722bfd42690d9d14d316c3df6ed5678d2f2121c37ee27f8326cfe270bbe3a96892d6809e5b71e46b203e7e3a74913eeae64ef784efcc3a267889c28aa2b7c'),
('Donald', 'donald@example.com', 'b938f24d56f6ce1e511b6701746798964ae551f9015957e27651a67ebd239ccb', 'f29722bfd42690d9d14d316c3df6ed5678d2f2121c37ee27f8326cfe270bbe3a96892d6809e5b71e46b203e7e3a74913eeae64ef784efcc3a267889c28aa2b7c')
;

INSERT INTO categories (name)
VALUES
('fabrics'), 
('yarns'),
('bobbin lace'),
('hobby crafts')
;

INSERT INTO subcategories (name)
VALUES
('stretch'),
('college'),
('wool'),
('cotton'),
('bobbin pillows'),
('bobbins'),
('card supplies'),
('decorations')
;

INSERT INTO listings (title, brand, img, length, unit, color, description, price, fk_categories_id, fk_subcategories_id, fk_users_id, added, longitude, latitude)
VALUES 
('Winters heart stretch fabric, 2m', 'Nuppu print company', '/static/media/no-image.a91e0603dcf39da028e6.png', 200, 'cm', 'blue', 'Very nice unwashed fabric', 50, 1, 1, (SELECT id FROM users WHERE username = 'Minnie'), '2022-05-24 14:49:32.177236+03', 23.782215, 61.488341),
('Dinosaur college fabric, 0,5m', null, '/static/media/no-image.a91e0603dcf39da028e6.png', 50, 'cm', 'grey', 'Only a little left', 10, 1, 2, (SELECT id FROM users WHERE username = 'Minnie'), '2022-06-02 22:14:35.027167+03', 23.782215, 61.488341),
('Box full of bobbins, about 50 pcs', null, '/static/media/no-image.a91e0603dcf39da028e6.png', 50, 'pcs', 'wood', 'Petite bobbins made from birch wood, about 12 cm long', 49, 3, 6, (SELECT id FROM users WHERE username = 'Goofy'), '2022-05-20 14:10:07.630984+03', 23.760335, 61.311971),
('7 Brothers multicolor yarn', 'Novita', '/static/media/no-image.a91e0603dcf39da028e6.png', 200, 'g', 'multi-color', 'Enough for a pair of wool socks', 10, 2, 3, (SELECT id FROM users WHERE username = 'Goofy'), '2022-06-02 21:58:24.438567+03', 23.760335, 61.311971),
('Misc collection of baby merino yarn', 'Drops', '/static/media/no-image.a91e0603dcf39da028e6.png', 500, 'g', 'multi-color', 'Very good for baby knits', 35, 2, 3, (SELECT id FROM users WHERE username = 'Goofy'), '2022-06-02 22:03:10.114006+03', 23.760335, 61.311971),
('Green and pink Tennessee yarn', 'Novita', '/static/media/no-image.a91e0603dcf39da028e6.png', 100, 'g', 'multi-color', 'In good condition', 5, 2, 4, (SELECT id FROM users WHERE username = 'Goofy'), '2022-06-02 22:12:51.868073+03', 23.760335, 61.311971),
('Set of wax crayons for kids', null, '/static/media/no-image.a91e0603dcf39da028e6.png', 10, 'pcs', 'multi-color', 'Used but in good condition', 2, 4, 7, (SELECT id FROM users WHERE username = 'Pluto'), '2022-05-20 14:10:07.630984+03', 23.650565, 61.521263),
('fighting fish strech fabric, 2m', null, '/static/media/no-image.a91e0603dcf39da028e6.png', 2, 'cm', 'multi-color', 'Unwashed quality fabric', 34, 1, 1, (SELECT id FROM users WHERE username = 'Pluto'), '2022-06-02 22:14:35.027167+03', 23.650565, 61.521263)
;

INSERT INTO bids (fk_listings_id, fk_users_id, bid_time)
VALUES 
((SELECT id FROM listings WHERE title = 'Set of wax crayons for kids'), (SELECT id FROM users WHERE username = 'Goofy'), '2022-05-22 21:24:36.187188+03'),
((SELECT id FROM listings WHERE title = 'Winters heart stretch fabric, 2m'), (SELECT id FROM users WHERE username = 'Goofy'), '2022-05-23 10:02:46.769041+03'),
((SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m'), (SELECT id FROM users WHERE username = 'Minnie'), '2022-05-23 10:02:46.769041+03'),
((SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m'), (SELECT id FROM users WHERE username = 'Donald'), '2022-05-23 10:03:28.765181+03'),
((SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m'), (SELECT id FROM users WHERE username = 'Goofy'), '2022-06-02 22:30:56.775758+03'),
((SELECT id FROM listings WHERE title = 'Box full of bobbins, about 50 pcs'), (SELECT id FROM users WHERE username = 'Minnie'), '2022-06-02 22:35:56.775758+03'),
((SELECT id FROM listings WHERE title = 'Box full of bobbins, about 50 pcs'), (SELECT id FROM users WHERE username = 'Pluto'), '2022-06-02 23:28:56.775758+03')
;

INSERT INTO conversations (fk_listings_id, fk_sender_id, fk_recipient_id, updated_at)
VALUES
((SELECT id FROM listings WHERE title = 'Set of wax crayons for kids'), (SELECT id FROM users WHERE username = 'Minnie'), (SELECT id FROM users WHERE username = 'Pluto'), '2022-06-02 23:59:56.775758+03'),
((SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m'), (SELECT id FROM users WHERE username = 'Goofy'), (SELECT id FROM users WHERE username = 'Pluto'), '2022-06-03 23:59:56.775758+03'),
((SELECT id FROM listings WHERE title = 'Winters heart stretch fabric, 2m'), (SELECT id FROM users WHERE username = 'Pluto'), (SELECT id FROM users WHERE username = 'Minnie'), '2022-06-01 19:59:56.775758+03')
;

INSERT INTO messages (message, sent_at, fk_conversations_id, fk_sender_id, fk_recipient_id)
VALUES
('Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '2022-06-02 23:28:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Set of wax crayons for kids') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Minnie')), (SELECT id FROM users WHERE username = 'Minnie'), (SELECT id FROM users WHERE username = 'Pluto')),
('Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2022-06-02 23:35:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Set of wax crayons for kids') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Minnie')), (SELECT id FROM users WHERE username = 'Pluto'), (SELECT id FROM users WHERE username = 'Minnie')),
('It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '2022-06-02 23:37:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Set of wax crayons for kids') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Minnie')), (SELECT id FROM users WHERE username = 'Minnie'), (SELECT id FROM users WHERE username = 'Pluto')),
('It was popularised in the 1960s.', '2022-06-02 23:45:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Set of wax crayons for kids') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Minnie')), (SELECT id FROM users WHERE username = 'Pluto'), (SELECT id FROM users WHERE username = 'Minnie')),
('Contrary to popular belief, Lorem Ipsum is not simply random text.', '2022-06-02 23:59:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Set of wax crayons for kids') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Minnie')), (SELECT id FROM users WHERE username = 'Minnie'), (SELECT id FROM users WHERE username = 'Pluto')),
('Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '2022-06-02 21:28:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Goofy')), (SELECT id FROM users WHERE username = 'Goofy'), (SELECT id FROM users WHERE username = 'Pluto')),
('It was popularised in the 1960s.', '2022-06-03 23:59:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'fighting fish strech fabric, 2m') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Goofy')), (SELECT id FROM users WHERE username = 'Pluto'), (SELECT id FROM users WHERE username = 'Goofy')),
('Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '2022-05-29 23:28:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Winters heart stretch fabric, 2m') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Pluto')), (SELECT id FROM users WHERE username = 'Pluto'), (SELECT id FROM users WHERE username = 'Minnie')),
('It was popularised in the 1960s.', '2022-06-01 19:59:56.775758+03', (SELECT id from CONVERSATIONS WHERE fk_listings_id= (SELECT id FROM listings WHERE title = 'Winters heart stretch fabric, 2m') AND fk_sender_id = (SELECT id FROM users WHERE username = 'Pluto')), (SELECT id FROM users WHERE username = 'Minnie'), (SELECT id FROM users WHERE username = 'Pluto'))
;