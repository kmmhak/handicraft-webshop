import devConfig from "./devConfig.js";
import proConfig from "./proConfig.js";
import pkg from "pg";

const { Pool } = pkg;

const pool = new Pool(
  process.env.NODE_ENV === "production" ? proConfig : devConfig
);

export default pool;
