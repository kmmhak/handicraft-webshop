import { config } from "dotenv";

if (process.env.NODE_ENV === "dev" || process.env.NODE_ENV === undefined) {
  console.log(process.env.NODE_ENV);
  config();
}

const devConfig = {
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
};

export default devConfig;
