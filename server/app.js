import express from "express";
import cors from "cors";
import authRouter from "./src/routes/auth.router.js";
import userRouter from "./src/routes/user.router.js";
import listingRouter from "./src/routes/listing.router.js";
import bidRouter from "./src/routes/bid.router.js";
import conversationRouter from "./src/routes/conversation.router.js";
import path from "path";
import { config } from "dotenv";

if (process.env.NODE_ENV === "dev" || process.env.NODE_ENV === undefined) {
  console.log(process.env.NODE_ENV);
  config();
}

const server = express();
let port = process.env.PORT;
if (port == null || port == "") {
  port = 5000;
}

server.use(express.static(path.resolve("../frontend/build")));
server.use(cors());
server.use(express.json());

server.use(authRouter);
server.use("/users", userRouter);
server.use("/listings", listingRouter);
server.use("/bids", bidRouter);
server.use("/conversations", conversationRouter);
server.get("*", (req, res) => {
  res.sendFile(path.resolve("../frontend/build", "index.html"));
});

server.listen(port, () => {
  console.log(`Server listening on port ${port}...`);
});
