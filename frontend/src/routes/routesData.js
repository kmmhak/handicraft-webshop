import Home from "../components/Home";
import Register from "../components/Register";
import Login from "../components/Login";
import Listing from "../components/Listing";
import UserPage from "../components/user/UserPage";
import AddListing from "../components/AddListing";
import UpdateUserInfo from "../components/user/UpdateUserInfo";
import UpdateUserPassword from "../components/user/UpdateUserPassword";
import DeleteUser from "../components/user/DeleteUser";
import EditListing from "../components/EditListing";
import Conversation from "../components/Conversation";
import Message from "../components/Message";
import Category from "../components/categories/Category";
import Subcategory from "../components/categories/Subcategory";

export const routes = [
  {
    id: 1,
    path: "/",
    element: <Home />,
  },
  {
    id: 2,
    path: "/register",
    element: <Register />,
  },
  {
    id: 3,
    path: "/login",
    element: <Login />,
  },
  {
    id: 4,
    path: "/listings/:id",
    element: <Listing />,
  },
  {
    id: 5,
    path: "/users/:id",
    element: <UserPage />,
  },
  {
    id: 6,
    path: "/categories/:category",
    element: <Category />,
  },
  {
    id: 7,
    path: "/subcategories/:subcategory",
    element: <Subcategory />,
  },
];

export const protectedRoutes = [
  {
    id: 1,
    path: "/listings",
    element: <AddListing />,
  },
  {
    id: 2,
    path: "/users/update",
    element: <UpdateUserInfo />,
  },
  {
    id: 3,
    path: "/users/change",
    element: <UpdateUserPassword />,
  },
  {
    id: 4,
    path: "/users/delete/",
    element: <DeleteUser />,
  },
  {
    id: 5,
    path: "/users/conversations/",
    element: <Conversation />,
  },
  {
    id: 6,
    path: "/users/conversations/:id",
    element: <Message />,
  },
  {
    id: 7,
    path: "/listings/edit/:id",
    element: <EditListing />,
  },
];
