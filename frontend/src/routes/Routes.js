import { Routes, Route } from "react-router-dom";
import { routes, protectedRoutes } from "./routesData";

const Router = () => {
  return (
    <Routes>
      {routes.map(({ path, element, id }) => (
        <Route path={path} element={element} key={id} />
      ))}

      {protectedRoutes.map(({ path, element, id }) => (
        <Route path={path} element={element} key={id} />
      ))}
    </Routes>
  );
};

export default Router;
