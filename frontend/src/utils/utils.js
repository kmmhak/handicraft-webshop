export const categories = [
  { id: 1, name: "fabrics" },
  { id: 2, name: "yarns" },
  { id: 3, name: "bobbin lace" },
  { id: 4, name: "hobby crafts" },
];

export const subcategories = [
  { id: 1, name: "stretch" },
  { id: 2, name: "college" },
  { id: 3, name: "wool" },
  { id: 4, name: "cotton" },
  { id: 5, name: "bobbin pillows" },
  { id: 6, name: "bobbins" },
  { id: 7, name: "card supplies" },
  { id: 8, name: "decorations" },
];
