import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Card from "../UI_components/Card";

const Category = () => {
  const { category } = useParams();

  const [listings, setListings] = useState([]);

  useEffect(() => {
    axios
      .get(`/listings/categories/${category}`)
      .then(function (response) {
        setListings(response.data.listings);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [category]);

  if (listings.length === 0) {
    return (
      <div>
        <h1>{category.toUpperCase()}</h1>
        <p className="paragraph_wrapper">Nothing listed in this category yet</p>
      </div>
    );
  } else {
    return (
      <div>
        <h1>{category.toUpperCase()}</h1>
        <div className="wrapper">
          {listings.map((listing) => (
            <Card key={listing.id} cardData={listing} />
          ))}
        </div>
      </div>
    );
  }
};

export default Category;
