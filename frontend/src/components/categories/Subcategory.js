import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Card from "../UI_components/Card";

const Subcategory = () => {
  const { subcategory } = useParams();

  const [listings, setListings] = useState([]);

  useEffect(() => {
    axios
      .get(`/listings/subcategories/${subcategory}`)
      .then(function (response) {
        setListings(response.data.listings);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [subcategory]);

  if (listings.length === 0) {
    return (
      <div>
        <h1>{subcategory.toUpperCase()}</h1>
        <p className="paragraph_wrapper">
          Nothing listed in this subcategory yet
        </p>
      </div>
    );
  } else {
    return (
      <div>
        <div className="newest">
          <div className="wrapper">
            {listings.map((listing) => (
              <Card key={listing.id} cardData={listing} />
            ))}
          </div>
        </div>
      </div>
    );
  }
};

export default Subcategory;
