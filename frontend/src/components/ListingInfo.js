import { Link } from "react-router-dom";
import Map from "./Map";

const ListingInfo = ({ listing, bids }) => {
  return (
    <div>
      <h1>{listing.title}</h1>
      <div className="wrapper">
        <img className="listing__image" src={listing.img} alt={listing.title} />
        <br></br>
      </div>
      <div className="wrapper">
        <div className="listing__details">
          {listing.brand !== null && <p>Brand: {listing.brand}</p>}
          <p>
            Qty: {listing.length} {listing.unit}
          </p>
          {listing.color !== null && <p>Color: {listing.color}</p>}
          <p>
            Seller:{" "}
            <Link to={`/users/${listing.user_id}`}>{listing.username}</Link>
          </p>
          <p>Category: {listing.category}</p>
          <p>Subcategory: {listing.subcategory}</p>
          <p>
            <strong>Price:</strong> {listing.price} €
          </p>
          <p>Description:</p>
          <p>{listing.description}</p>
          <p>Bids for this item: {bids}</p>
        </div>
        <div>
          <Map center={[listing.latitude, listing.longitude]} />
        </div>
      </div>
    </div>
  );
};

export default ListingInfo;
