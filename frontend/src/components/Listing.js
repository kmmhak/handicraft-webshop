import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Button from "./UI_components/Button";
import UserCard from "./UI_components/UserCard";
import { useUser } from "../contexts/UserContext";
import inputs from "../formInputData/messageInput.js";
import FormInput from "./FormInput";
import ListingInfo from "./ListingInfo";

const Listing = () => {
  let { id } = useParams();
  const { currentUser } = useUser();
  const [listing, setListing] = useState([]);
  const [bids, setBids] = useState(0);
  const [newBid, setNewBid] = useState(false);
  const [bidders, setBidders] = useState([]);
  const [message, setMessage] = useState("");
  const [formVisible, setFormVisible] = useState(false);
  const [biddingId, setBiddingId] = useState(0);
  const [hasMadeBid, setHasMadeBid] = useState(false);
  const navigate = useNavigate();
  const token = localStorage.getItem("token");
  const isOwnListing = Number(currentUser?.id) === Number(listing.user_id);

  useEffect(() => {
    axios
      .get(`/listings/${id}`)
      .then(function (response) {
        setListing(response.data.listings[0]);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [id]);

  useEffect(() => {
    axios
      .get(`/bids/listing/${id}`)
      .then((response) => {
        setBids(response.data.bids.length);
        setBidders(response.data.bids);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [newBid, bids, id]);

  useEffect(() => {
    const foundBidder = bidders.find(
      (bidder) => Number(bidder.fk_users_id) === Number(currentUser.id)
    );
    foundBidder ? setHasMadeBid(true) : setHasMadeBid(false);
  }, [bidders, currentUser.id]);

  const makeBid = () => {
    axios
      .post(
        `/bids/listing/${id}`,
        {},
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        setNewBid(!newBid);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(
        `/conversations/listing/${id}`,
        {
          fk_listings_id: listing.id,
          fk_recipient_id: isOwnListing ? biddingId : listing.user_id,
          message: message,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        setBiddingId(0);
        setMessage("");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const navigateLogin = () => {
    navigate("/login");
  };

  const onChange = (e) => {
    setMessage(e.target.value);
  };

  if (listing.length !== 0) {
    return (
      <div>
        <ListingInfo listing={listing} bids={bids} />

        {currentUser === null ? (
          <Button
            className="button"
            text="Login to make a bid or send a message to the seller"
            onClick={navigateLogin}
          />
        ) : currentUser.username !== listing.username ? (
          <div>
            <center>
              {hasMadeBid ? (
                <p className="paragraph_wrapper">
                  You have already made a bid on this listing
                </p>
              ) : (
                <Button
                  className="button"
                  text="Make a bid"
                  onClick={makeBid}
                />
              )}
            </center>
            <form onSubmit={handleSubmit}>
              <h3>Ask for more details</h3>
              {inputs.map((input) => (
                <FormInput
                  key={input.id}
                  {...input}
                  value={message}
                  onChange={onChange}
                />
              ))}
              <Button className="button" text="Send message" />
            </form>
          </div>
        ) : (
          <div>
            <div>
              {(() => {
                if (bids > 0) {
                  return (
                    <div>
                      <div>
                        <br></br>
                        <h1>Bids</h1>
                        <div className="small__wrapper">
                          {bidders.map((bidder) => (
                            <UserCard
                              key={bidder.fk_users_id}
                              cardData={bidder}
                              bidders={bidders}
                              formVisible={formVisible}
                              setFormVisible={setFormVisible}
                              setBiddingId={setBiddingId}
                            />
                          ))}
                        </div>
                      </div>
                      {(() => {
                        if (biddingId !== 0) {
                          return (
                            <div className="form">
                              <form onSubmit={handleSubmit}>
                                <h3>Send message</h3>
                                {inputs.map((input) => (
                                  <FormInput
                                    key={input.id}
                                    {...input}
                                    value={message}
                                    onChange={onChange}
                                  />
                                ))}
                                <Button
                                  className="button"
                                  text="Send message"
                                />
                              </form>
                            </div>
                          );
                        }
                      })()}
                    </div>
                  );
                } else {
                  return (
                    <div>
                      <br></br>
                      <h1>Bids</h1>
                      <p className="paragraph_wrapper">
                        No bids on your listing yet
                      </p>
                    </div>
                  );
                }
              })()}
            </div>
          </div>
        )}
      </div>
    );
  } else {
    return <h1>Loading...</h1>;
  }
};

export default Listing;
