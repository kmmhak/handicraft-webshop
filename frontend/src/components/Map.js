import { MapContainer, TileLayer, Circle } from "react-leaflet";

function Map({ center }) {
  return (
    <MapContainer center={center} zoom={12} scrollWheelZoom={true}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      <Circle center={center} radius={500} />
    </MapContainer>
  );
}

export default Map;
