import React, { useState, useEffect } from "react";
import SmallCard from "../UI_components/SmallCard";
import axios from "axios";

const OtherUserPage = ({ id, userListings }) => {
  const [user, setUser] = useState({});

  useEffect(() => {
    axios
      .get(`/users/${id}`)
      .then(function (response) {
        setUser(response.data.user[0]);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  return (
    <div>
      <h1>User {user.username}'s profile page</h1>
      <h2>{user.username}'s listings</h2>
      {userListings.length > 0 ? (
        <div className="small__wrapper">
          {userListings.map((listing) => (
            <SmallCard key={listing.id} cardData={listing} />
          ))}
        </div>
      ) : (
        <p align="center">User has not listed anything yet</p>
      )}
    </div>
  );
};

export default OtherUserPage;
