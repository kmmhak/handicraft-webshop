import { useUser } from "../../contexts/UserContext";
import { useState } from "react";
import FormInput from "../FormInput";
import Button from "../UI_components/Button";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const UpdateUserPassword = () => {
  const navigate = useNavigate();
  const { currentUser } = useUser();
  const token = localStorage.getItem("token");

  const [values, setValues] = useState({
    password: "",
    password2: "",
    oldPassword: "",
  });

  const inputs = [
    {
      id: 1,
      name: "password",
      type: "password",
      placeholder: "type in new password",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number and 1 special character",
      label: "Password",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
    {
      id: 2,
      name: "password2",
      type: "password",
      placeholder: "repeat new password",
      errorMessage: "passwords do not match",
      label: "Repeat new password",
      pattern: values.password,
      required: true,
    },
    {
      id: 3,
      name: "oldPassword",
      type: "password",
      placeholder: "confirm change with password",
      errorMessage: "Type in current password to confirm change",
      label: "Confirmation",
      required: true,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .patch(
        `/users/change`,
        {
          newPassword: values.password,
          newPassword2: values.password2,
          oldPassword: values.oldPassword,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        navigate(`/users/${currentUser.id}`);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <h1>Change password</h1>

      <div className="form">
        <form onSubmit={handleSubmit}>
          {inputs.map((input) => (
            <FormInput
              key={input.id}
              {...input}
              value={values[input.name]}
              onChange={onChange}
            />
          ))}
          <Button className="button" text="Change password" />
        </form>
      </div>
    </div>
  );
};

export default UpdateUserPassword;
