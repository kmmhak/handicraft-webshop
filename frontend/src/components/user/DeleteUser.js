import FormInput from "../FormInput";
import Button from "../UI_components/Button";
import { useState } from "react";
import axios from "axios";
import { useUser } from "../../contexts/UserContext";
import { useNavigate } from "react-router-dom";

const DeleteUser = () => {
  const [password, setPassword] = useState("");
  const { currentUser, logout } = useUser();
  const id = currentUser.id;
  const token = localStorage.getItem("token");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(
        "/users/verify",
        { password: password },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        if (response.status === 200) {
          axios
            .delete(`/users/${id}`, {
              headers: {
                Authorization: token,
              },
            })
            .then((response) => {
              logout();
              navigate("/");
            })
            .catch((error) => {
              console.log(error);
            });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setPassword(e.target.value);
  };

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <h1>Delete your user account</h1>
        <FormInput
          name="password"
          type="text"
          placeholder="confirm with password"
          label="Confirmation"
          required={true}
          errorMessage="Password confirmation required"
          onChange={onChange}
        />
        <Button className="button" text="Delete user" />
      </form>
    </div>
  );
};

export default DeleteUser;
