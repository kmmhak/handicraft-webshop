import { useUser } from "../../contexts/UserContext";
import { useState } from "react";
import FormInput from "../FormInput";
import Button from "../UI_components/Button";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const UpdateUserInfo = () => {
  const navigate = useNavigate();
  const { token, currentUser, login } = useUser();

  const [values, setValues] = useState({
    email: "",
    password: "",
  });

  const inputs = [
    {
      id: 1,
      name: "email",
      type: "email",
      placeholder: "type in new email address",
      errorMessage: "Email should be a valid email address",
      label: "New email",
      required: true,
    },
    {
      id: 2,
      name: "password",
      type: "password",
      placeholder: "confirm change with password",
      errorMessage: "Type in current password to confirm change",
      label: "Password",
      required: true,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .patch(
        "/users/update",
        {
          email: values.email,
          password: values.password,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        axios
          .post("/login", {
            email: values.email,
            password: values.password,
          })
          .then(function (response) {
            const { token, user } = response.data;
            login(user, token);
            navigate(`/users/${currentUser.id}`);
          });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <div>
      <h1> Change email address</h1>

      <div className="form">
        <form onSubmit={handleSubmit}>
          <FormInput
            name="oldEmail"
            type="text"
            placeholder={`${currentUser.email}`}
            label="Current email address"
            disabled={true}
          />
          {inputs.map((input) => (
            <FormInput
              key={input.id}
              {...input}
              value={values[input.name]}
              onChange={onChange}
            />
          ))}
          <Button className="button" text="Change email address" />
        </form>
      </div>
    </div>
  );
};

export default UpdateUserInfo;
