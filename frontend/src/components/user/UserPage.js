import React, { useState, useEffect } from "react";
import { useUser } from "../../contexts/UserContext";
import { Link, useParams } from "react-router-dom";
import SmallCard from "../UI_components/SmallCard";
import axios from "axios";
import Button from "../UI_components/Button";
import OtherUserPage from "./OtherUserPage";

const UserPage = () => {
  const { currentUser } = useUser();
  const [userListings, setUserListings] = useState([]);
  const [userBids, setUserBids] = useState([]);
  const { id } = useParams();

  const isOwnProfile = Number(currentUser?.id) === Number(id);

  useEffect(() => {
    axios
      .get(`/listings/user/${id}`)
      .then(function (response) {
        setUserListings(response.data.userListings);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  useEffect(() => {
    axios
      .get(`/bids/${id}`)
      .then(function (response) {
        setUserBids(response.data.foundBids);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  return (
    <div>
      {!isOwnProfile ? (
        <OtherUserPage id={id} userListings={userListings} />
      ) : (
        <div>
          <center>
            <h1>Welcome to your homepage {currentUser.username}!</h1>
            <Link to={"/users/conversations"}>
              <Button className="button" text="Messages"></Button>
            </Link>
            <br></br>
            <Link to={"/users/update"}>
              <Button className="button" text="Update email"></Button>
            </Link>
            <br></br>
            <Link to={"/users/change"}>
              <Button className="button" text="Update password"></Button>
            </Link>

            <br></br>
            <Link to={"/users/delete/"}>
              <Button className="button" text="Delete user account"></Button>
            </Link>
          </center>
          <h2>Your listings</h2>
          {userListings?.length > 0 ? (
            <div className="small__wrapper">
              {userListings.map((listing) => (
                <SmallCard key={listing.id} cardData={listing} />
              ))}
            </div>
          ) : (
            <p className="paragraph_wrapper">
              You have not listed anything yet
            </p>
          )}

          <h2>Listings you have made bids on</h2>
          {userBids?.length > 0 ? (
            <div className="small__wrapper">
              {userBids.map((bid) => (
                <SmallCard key={bid.id} cardData={bid} />
              ))}
            </div>
          ) : (
            <p className="paragraph_wrapper">
              You have not made bids on any listings yet
            </p>
          )}
        </div>
      )}
    </div>
  );
};

export default UserPage;
