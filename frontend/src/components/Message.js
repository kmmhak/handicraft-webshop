import axios from "axios";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import MessageBox from "./UI_components/MessageBox";
import FormInput from "./FormInput";
import Button from "./UI_components/Button";
import { useUser } from "../contexts/UserContext";

const Message = () => {
  const { currentUser } = useUser();
  let { id } = useParams();
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState("");
  const [newMessage, setNewMessage] = useState(false);
  const token = localStorage.getItem("token");

  useEffect(() => {
    axios
      .get(`/conversations/${id}`, {
        headers: {
          Authorization: token,
        },
      })
      .then(function (response) {
        setMessages(response.data.message);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [token, id, newMessage]);

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(
        `/conversations`,
        {
          message: message,
          fk_conversations_id: messages[0].fk_conversations_id,
          fk_recipient_id:
            currentUser.id === messages[0].fk_recipient_id
              ? messages[0].fk_sender_id
              : messages[0].fk_recipient_id,
        },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        setNewMessage(!newMessage);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setMessage(e.target.value);
  };

  if (messages.length === 0) {
    return <p className="paragraph_wrapper">No messages yet </p>;
  } else {
    return (
      <div>
        <div className="message__wrapper">
          {messages.map((message) => (
            <MessageBox key={message.id} cardData={message} />
          ))}
          <form onSubmit={handleSubmit}>
            <FormInput onChange={onChange} />
            <Button className="button" text="Send Message" />
          </form>
        </div>
      </div>
    );
  }
};

export default Message;
