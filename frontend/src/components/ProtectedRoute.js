import { useNavigate, Outlet } from "react-router-dom";
import { useUser } from "../contexts/UserContext";
import React, { useEffect, useState } from "react";
import axios from "axios";

const ProtectedRoute = () => {
  const { login } = useUser();
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) navigate("/login");

    axios
      .get("/authenticate", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        const { user } = response.data;
        login(user, token);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        navigate("/login");
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return !isLoading ? <Outlet /> : <p>Loading...</p>;
};

export default ProtectedRoute;
