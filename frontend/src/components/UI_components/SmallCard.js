import Button from "./Button";
import { Link } from "react-router-dom";
import React from "react";
import axios from "axios";
import { useUser } from "../../contexts/UserContext";

const SmallCard = ({ cardData }) => {
  const id = cardData.id;
  const token = localStorage.getItem("token");
  const { currentUser } = useUser();

  const isOwnProfile = Number(currentUser?.id) === Number(cardData.fk_users_id);

  const deleteListing = () => {
    axios
      .delete(`/listings/${id}`, {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {})
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="card">
      <div className="card__body">
        <img src={cardData.img} alt={cardData.title} className="card__image" />
        <h3 className="card__title">{cardData.title}</h3>
      </div>
      <Link to={`/listings/${cardData.id}`}>
        <Button className="button" text="View item"></Button>
      </Link>
      {isOwnProfile ? (
        <div>
          <Link to={`/listings/edit/${cardData.id}`}>
            <Button className="button" text="Edit item"></Button>
          </Link>
          <Button
            className="button"
            text="Delete item"
            onClick={deleteListing}
          />
        </div>
      ) : null}
    </div>
  );
};

/*
 */

export default SmallCard;
