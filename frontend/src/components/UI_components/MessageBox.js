import { useUser } from "../../contexts/UserContext";

const MessageBox = ({ cardData }) => {
  const { currentUser } = useUser();

  return (
    <div className="messagecard">
      <div className="messagecard__body">
        <p className="messagecard__info">
          {Number(currentUser.id) === cardData.fk_sender_id ? (
            <strong>{cardData.recipient_username}</strong>
          ) : (
            <strong>{cardData.sender_username}</strong>
          )}{" "}
          {cardData.sent_at.substring(0, 10) +
            " " +
            cardData.sent_at.substring(11, 19)}
        </p>
        <p className="messagecard__msg">{cardData.message}</p>
      </div>
    </div>
  );
};

export default MessageBox;
