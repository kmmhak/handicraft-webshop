import * as GiIcons from "react-icons/gi";
import * as ImIcons from "react-icons/im";

const SidebarData = [
  {
    id: 1,
    title: "Fabrics",
    path: "/categories/fabrics",
    icon: <GiIcons.GiRolledCloth />,
    subNav: [
      {
        id: 1,
        title: "Stretch",
        path: "/subcategories/stretch",
      },
      {
        id: 2,
        title: "College",
        path: "/subcategories/college",
      },
    ],
  },
  {
    id: 2,
    title: "Yarns",
    path: "/categories/yarns",
    icon: <GiIcons.GiYarn />,
    subNav: [
      {
        id: 3,
        title: "Cotton",
        path: "/subcategories/cotton",
      },
      {
        id: 4,
        title: "Wool",
        path: "/subcategories/wool",
      },
    ],
  },
  {
    id: 3,
    title: "Bobbin lace",
    path: "/categories/bobbin%20lace",
    icon: <ImIcons.ImScissors />,
    subNav: [
      {
        id: 5,
        title: "Bobbin pillows",
        path: "/subcategories/bobbin%20pillows",
      },
      {
        id: 6,
        title: "Bobbins",
        path: "/subcategories/bobbins",
      },
    ],
  },
  {
    id: 4,
    title: "Hobby crafts",
    path: "/categories/hobby%20crafts",
    icon: <GiIcons.GiPaintBrush />,
    subNav: [
      {
        id: 7,
        title: "Card Supplies",
        path: "/subcategories/card%20supplies",
      },
      {
        id: 8,
        title: "Decorations",
        path: "/subcategories/decorations",
      },
    ],
  },
];

export default SidebarData;
