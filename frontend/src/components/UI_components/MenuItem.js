import { Link } from "react-router-dom";
import SubMenuItem from "./SubMenuItem";

const MenuItem = ({ item }) => {
  return (
    <div>
      <li key={item.key} className="nav-text">
        <Link to={item.path}>
          {item.icon}
          <div className="nav-text-title">{item.title}</div>
        </Link>
      </li>
      <SubMenuItem item={item.subNav} />
    </div>
  );
};

export default MenuItem;
