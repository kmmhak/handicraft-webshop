import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useUser } from "../../contexts/UserContext";
import { useEffect } from "react";
import axios from "axios";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import MenuItem from "./MenuItem";
import Button from "./Button";
import SidebarData from "./SidebarData";

import { IconContext } from "react-icons";

function Sidebar() {
  const { loggedIn, logout, currentUser, login } = useUser();
  const token = localStorage.getItem("token");
  const navigate = useNavigate();

  const [sidebar, setSidebar] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);

  useEffect(() => {
    if (token && !loggedIn) {
      axios
        .get("/authenticate", {
          headers: {
            Authorization: token,
          },
        })
        .then((response) => {
          const { user } = response.data;
          login(user, token);
        })
        .catch((error) => {
          console.log(error);
          navigate("/login");
        });
    }
  }, [loggedIn, token, login, navigate]);

  return (
    <>
      <IconContext.Provider value={{ color: "#fff" }}>
        <div className="sidebar">
          <Link to="#" className="menu-bars">
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
          <Link to="/" className="menu-bars">
            <AiIcons.AiFillHome />
          </Link>
          {loggedIn ? (
            <>
              <Link to={`/listings`}>
                <Button className="navbar__btn" text="New listing"></Button>
              </Link>

              <Link to={"/"}>
                <Button
                  className="navbar__btn"
                  text="Logout"
                  onClick={logout}
                ></Button>
              </Link>

              <Link to={`/users/${currentUser.id}`}>
                <Button
                  className="navbar__btn"
                  text={<AiIcons.AiOutlineUser size={28} />}
                ></Button>
              </Link>
            </>
          ) : (
            <>
              <Link to={`/register`}>
                <Button className="navbar__btn" text="Register"></Button>
              </Link>

              <Link to={`/login`}>
                <Button className="navbar__btn" text="Login"></Button>
              </Link>
            </>
          )}
        </div>
        <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
          <ul className="nav-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              <Link to="#" className="menu-bars">
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index) => {
              return (
                <div key={index}>
                  <MenuItem key={index} item={item} />
                </div>
              );
            })}
          </ul>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Sidebar;
