import Button from "./Button";
import { Link } from "react-router-dom";
import React from "react";

const UserCard = ({
  cardData,
  bidders,
  formVisible,
  setFormVisible,
  setBiddingId,
}) => {
  const index = bidders.map((bidder) => bidder.id).indexOf(cardData.id);

  const showForm = () => {
    setFormVisible(!formVisible);
    setBiddingId(cardData.fk_users_id);
  };

  return (
    <div className="card">
      <div className="card__body">
        <h3 className="card__title">{cardData.username}</h3>
        {index === 0 ? (
          <p className="card__price"> First bidder</p>
        ) : (
          <p className="card__price">In queue: {index}</p>
        )}
      </div>
      <Link to={`/users/${cardData.fk_users_id}`}>
        <Button className="button" text="View profile"></Button>
      </Link>
      <Button
        className="button"
        text="Send message"
        onClick={showForm}
      ></Button>
    </div>
  );
};

export default UserCard;
