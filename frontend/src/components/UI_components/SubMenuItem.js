import { Link } from "react-router-dom";

const SubMenuItem = ({ item }) => {
  return (
    <div>
      <ul>
        {item.map((submenu) => (
          <li key={submenu.id} className="nav-text">
            <Link to={submenu.path}>
              <div className="nav-text-title-sub">{submenu.title}</div>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SubMenuItem;
