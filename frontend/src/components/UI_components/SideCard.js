import { useUser } from "../../contexts/UserContext";
import { useNavigate } from "react-router-dom";

const SideCard = ({ cardData }) => {
  const { currentUser } = useUser();
  const navigate = useNavigate();

  const navigateToMessages = () => {
    navigate(`/users/conversations/${cardData.conversations_id}`);
  };
  return (
    <div className="sidecard" onClick={navigateToMessages}>
      <img src={cardData.img} className="sidecard__img" alt={cardData.id} />
      <div className="sidecard__body">
        <p className="sidecard__title">Listing: {cardData.title}</p>
        {Number(currentUser.id) === cardData.fk_recipient_id ? (
          <p className="sidecard__info">{cardData.sender_username}</p>
        ) : (
          <p className="sidecard__info">{cardData.recipient_username}</p>
        )}
        <p className="sidecard__info">
          Latest message:{" "}
          {cardData.updated_at.substring(0, 10) +
            " " +
            cardData.updated_at.substring(11, 19)}
        </p>
        <p className="sidecard__msg">
          {cardData.message.length > 75
            ? cardData.message.slice(0, 75) + "..."
            : cardData.message}
        </p>
      </div>
    </div>
  );
};

export default SideCard;
