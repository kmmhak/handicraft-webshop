import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import FormInput from "./FormInput";
import Button from "./UI_components/Button";
import inputs from "../formInputData/listingInput.js";

const EditListing = () => {
  let { id } = useParams();
  const token = localStorage.getItem("token");
  const navigate = useNavigate();

  const [values, setValues] = useState({
    title: "",
    brand: "",
    img: "",
    length: "",
    unit: "",
    color: "",
    description: "",
    price: "",
    category: "",
    subcategory: "",
    latitude: "",
    longitude: "",
  });

  useEffect(() => {
    axios
      .get(`/listings/${id}`)
      .then(function (response) {
        let {
          title,
          brand,
          img,
          length,
          unit,
          color,
          description,
          price,
          category_id,
          subcategory_id,
          latitude,
          longitude,
          // eslint-disable-next-line no-unused-vars
          ...rest
        } = response.data.listings[0];

        if (brand === null) {
          brand = "";
        }

        if (color === null) {
          color = "";
        }

        if (img === null) {
          img = "";
        }

        const previousValues = {
          title,
          brand,
          img,
          length,
          unit,
          color,
          description,
          price,
          category: category_id,
          subcategory: subcategory_id,
          latitude,
          longitude,
        };

        setValues(previousValues);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [id]);

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        `/listings/edit/${id}`,
        {
          title: values.title,
          brand: values.brand,
          img: values.img,
          length: Number(values.length),
          unit: values.unit,
          color: values.color,
          description: values.description,
          price: Number(values.price),
          category: Number(values.category),
          subcategory: Number(values.subcategory),
          latitude: Number(values.latitude),
          longitude: Number(values.longitude),
        },
        {
          headers: {
            Authorization: token,
          },
        }
      )
      .then(function (response) {
        navigate(`/listings/${id}`);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const handleLocationUpdate = () => {
    const geo = navigator.geolocation;
    geo.getCurrentPosition(
      (pos) => {
        setValues({
          ...values,
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude,
        });
      },
      (error) => console.log(error)
    );
  };

  return (
    <div>
      <div className="form">
        <form onSubmit={handleSubmit}>
          <FormInput
            {...inputs[0]}
            value={values[inputs[0].name]}
            onChange={onChange}
          />

          <FormInput
            {...inputs[1]}
            value={values[inputs[1].name]}
            onChange={onChange}
          />

          <div className="enum_wrapper">
            <select
              className="formInput"
              id="unit"
              name="unit"
              onChange={onChange}
            >
              <option value="">Choose units</option>
              <option value="g">g</option>
              <option value="cm">cm</option>
              <option value="pcs">pcs</option>
            </select>

            <select
              className="formInput"
              id="category"
              name="category"
              onChange={onChange}
            >
              <option value="">Choose category</option>
              <option value="1">Fabrics</option>
              <option value="2">Yarns</option>
              <option value="3">Bobbin lace</option>
              <option value="4">Hobby crafts</option>
            </select>

            <select
              className="formInput"
              id="subcategory"
              name="subcategory"
              onChange={onChange}
            >
              <option value="">Choose subcategory</option>
              {(() => {
                if (values.category === "1") {
                  return (
                    <>
                      <option value="1">Stretch</option>
                      <option value="2">College</option>
                    </>
                  );
                } else if (values.category === "2") {
                  return (
                    <>
                      <option value="3">Wool</option>
                      <option value="4">Cotton</option>
                    </>
                  );
                } else if (values.category === "3") {
                  return (
                    <>
                      <option value="5">Bobbin pillows</option>
                      <option value="6">Bobbins</option>
                    </>
                  );
                } else if (values.category === "4") {
                  return (
                    <>
                      <option value="7">Card supplies</option>
                      <option value="8">Decorations</option>
                    </>
                  );
                } else {
                  return <></>;
                }
              })()}
            </select>
          </div>

          <FormInput
            {...inputs[3]}
            value={values[inputs[3].name]}
            onChange={onChange}
          />

          <FormInput
            {...inputs[4]}
            value={values[inputs[4].name]}
            onChange={onChange}
          />

          <FormInput
            {...inputs[5]}
            value={values[inputs[5].name]}
            onChange={onChange}
          />

          <FormInput
            {...inputs[6]}
            value={values[inputs[6].name]}
            onChange={onChange}
          />
          <div className="shortformInput">
            <div>
              <FormInput
                {...inputs[7]}
                value={values[inputs[7].name]}
                onChange={onChange}
              />
              <FormInput
                {...inputs[8]}
                value={values[inputs[8].name]}
                onChange={onChange}
              />
            </div>
          </div>
          <Button className="button" text="Edit listing" />
        </form>
        <Button
          className="button"
          text="Update current position"
          onClick={handleLocationUpdate}
        />
      </div>
    </div>
  );
};

export default EditListing;
