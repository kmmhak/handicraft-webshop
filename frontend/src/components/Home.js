import Card from "./UI_components/Card";
import axios from "axios";
import { useEffect, useState } from "react";

const Home = () => {
  const [listings, setListings] = useState([]);

  useEffect(() => {
    axios.get(`/listings/newest`).then(function (response) {
      setListings(response.data.newest);
    });
  }, []);

  return (
    <div>
      <div className="wrapper">
        <h1 className="demo">THIS IS A DEMO, NOT A REAL APP! </h1>
      </div>
      <br></br>
      <h1>Welcome to Kiku's handicraft webshop!</h1>
      <div className="paragraph_wrapper">
        <p>
          Welcome to my full stack personal project that I worked on from April
          to July 2022 in Buutti Trainee Academy. It started out as a backend
          excercise and I thought I'd give React a go and try to make a simple
          frontend for the shop. The more I learned to make stuff with React the
          more I wanted to expand my app. I feel adding images, getting
          notifications when new messages arrive and a search tool to find
          interesting listings are the next bigger functionalities missing from
          the app if I decide to continue developing the app after the Trainee
          Academy. And yeah, purple is my favourite colour!
        </p>
        <br></br>
        <p>
          You can log in with any of the predefined users:
          <br></br>
          <br></br>minnie@example.com
          <br></br>pluto@example.com
          <br></br>goofy@example.com
          <br></br>donald@example.com
          <br></br>
          <br></br> with the password Password1! or register a new user.
        </p>
        <br></br>
        <p>
          The source code can be found in my{" "}
          <a
            href="https://gitlab.com/kmmhak/handicraft-webshop"
            target="_blank"
            rel="noreferrer"
          >
            Gitlab
          </a>
        </p>
        <br></br>
        <p>
          You can find out more about me and my contact information on{" "}
          <a
            href="https://www.linkedin.com/in/kristiina-hakkala/"
            target="_blank"
            rel="noreferrer"
          >
            LinkedIn
          </a>
        </p>
      </div>
      {listings.length !== 0 ? (
        <div>
          {" "}
          <h2>5 newest listings</h2>
          <div className="wrapper">
            {listings.map((listing) => (
              <Card key={listing.id} cardData={listing} />
            ))}
          </div>{" "}
        </div>
      ) : (
        <p className="paragraph_wrapper">Loading newest listings...</p>
      )}
    </div>
  );
};

export default Home;
