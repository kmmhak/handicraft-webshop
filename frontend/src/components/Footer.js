const Footer = () => {
  return (
    <div className="footer">
      <h2>© Kiku 2022</h2>
    </div>
  );
};

export default Footer;
