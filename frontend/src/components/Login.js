import { useState } from "react";
import FormInput from "./FormInput";
import axios from "axios";
import Button from "./UI_components/Button";
import { useNavigate } from "react-router-dom";
import { useUser } from "../contexts/UserContext";
import inputs from "../formInputData/loginInput.js";

const Login = () => {
  const navigate = useNavigate();
  const { login } = useUser();

  const [values, setValues] = useState({
    email: "",
    password: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post("/login", {
        email: values.email,
        password: values.password,
      })
      .then(function (response) {
        const { token, user } = response.data;
        login(user, token);
        navigate(`/users/${response.data.user[0].id}`);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <h1>Login</h1>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={values[input.name]}
            onChange={onChange}
          />
        ))}
        <Button className="button" text="Login" />
      </form>
    </div>
  );
};

export default Login;
