import { useState } from "react";

const FormInput = (props) => {
  const [focused, setFocused] = useState(false);
  const { label, errorMessage, onChange, id, ...inputProps } = props;

  const handleFocus = (e) => {
    setFocused(true);
  };

  return (
    <div className="formInput">
      <label>{label}</label>
      <input
        {...inputProps}
        onChange={onChange}
        onBlur={handleFocus}
        onFocus={() =>
          (inputProps.name === "password2" ||
            inputProps.name === "subcategory") &&
          setFocused(true)
        }
        focused={focused.toString()}
      />
      <p className="error">{errorMessage}</p>
    </div>
  );
};

export default FormInput;
