import axios from "axios";
import { useState, useEffect } from "react";
import SideCard from "./UI_components/SideCard";

const Message = () => {
  const [conversations, setConversations] = useState([]);
  const token = localStorage.getItem("token");

  useEffect(() => {
    const updateConversations = () => {
      axios
        .get(`/conversations`, {
          headers: {
            Authorization: token,
          },
        })
        .then(function (response) {
          setConversations(response.data.message);
        })
        .catch((error) => {
          console.log(error);
        });
    };

    updateConversations();
    const interval = setInterval(() => {
      updateConversations();
    }, 5000);
    return () => {
      clearInterval(interval);
    };
  }, [token]);

  if (conversations.length !== 0) {
    return (
      <div>
        <h1>Conversations</h1>

        <div className="sidecard__wrapper">
          {conversations.map((conversation) => (
            <SideCard
              key={conversation.conversations_id}
              cardData={conversation}
            />
          ))}
        </div>
      </div>
    );
  } else {
    return <h1 className="paragraph_wrapper">You have no messages yet</h1>;
  }
};

export default Message;
