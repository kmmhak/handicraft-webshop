import { BrowserRouter } from "react-router-dom";
import Sidebar from "./components/UI_components/Sidebar";
import Footer from "./components/Footer";
import Router from "./routes/Routes";
import { UserProvider } from "./contexts/UserContext";
import "./style.css";

function App() {
  return (
    <BrowserRouter>
      <UserProvider>
        <div className="container">
          <div className="content-wrap">
            <Sidebar />
            <Router />
          </div>
          <Footer />
        </div>
      </UserProvider>
    </BrowserRouter>
  );
}

export default App;
