export const inputs = [
  {
    id: 1,
    name: "email",
    type: "email",
    placeholder: "email",
    label: "Email",
    required: true,
  },
  {
    id: 2,
    name: "password",
    type: "password",
    placeholder: "password",
    label: "Password",
    required: true,
  },
];

export default inputs;
