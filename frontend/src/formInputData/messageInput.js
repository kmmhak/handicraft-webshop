export const inputs = [
  {
    id: 1,
    name: "message",
    type: "text",
    placeholder: "write message here",
  },
];

export default inputs;
