# Handicraft Webshop

A full stack marketplace for customers to sell and buy anything to do with handicrafts. A user can look through different product categories to find something interesting to buy. After registering and logging in a user can put products on sale, make bids on items and stay in queue for an item in case the previous buyer changes their mind. Users can send messages to the seller to ask questions about their wares and arrange for a meeting to pick up the items. The seller can contact the bidders and arrange for a meetup. There is a map (leaflet) on every listing indicating an approximate area where the item can be picked up.

The app can be viewed at https://handicraft-webshop.herokuapp.com/


# Endpoints

| Root endpoint | Method | Description            |
| ------------- | ------ | ---------------------- |
| /             | GET    | Front page of the shop |

| Root endpoint  | Method | Description   |
| -------------- | ------ | ------------- |
| /auth/register | POST   | Registeration |
| /auth/login    | POST   | Login         |
| /auth/authenticate | GET| Authenticate logged in user |

| Root endpoint | Method | Description             |
| ------------- | ------ | ----------------------- |
| /users/       | GET    | Get all users           |
| /users/{id}   | GET    | Get user by id          |
| /users/change | PATCH  | Change user password    |
| /users/update | PATCH  | Change user information |
| /users/{id}   | DELETE | Delete user account     |
| /users/{id}   | PATCH  | Update user role        |


| Root endpoint  | Method | Description           |
| -------------- | ------ | --------------------- |
| /listings/     | GET    | Get all listings      |
| /listings/{id} | GET    | Get a listing by id   |
| /listings/newest | GET    | Get a 5 newest listings   |
| /listings/categories/{category} | GET | Get all listings in a category |
| /listings/subcategories/{subcategory} | GET | Get all listings in a subcategory |
| /listings/     | POST   | Add a listing         |
| /listings/user/{id} | GET    | Get a user's listings |
| /listings/{id} | DELETE | Delete a listing      |
| /listings/edit/{id} | PUT | Update listings information |


| Root endpoint      | Method | Description              |
| ------------------ | ------ | ------------------------ |
| /bids/user/{id}    | GET    | Get a user's bids        |
| /bids/listing/{id} | POST   | Make a bid on a listing  |
| /bids/             | GET    | Get your own bid history |
| /bids/listing/{id} | GET    | Get a listing's bids |


| Root endpoint | Method | Description                             |
| ------------- | ------ | --------------------------------------- |
| /conversations/ | GET | Get a user's conversations |
| /conversations/{id}    | GET    | Get a user's sent and received messages |
| /conversations/    | POST   | Reply to a message                  |
| /conversations/listing/{id} | POST | Send a new message |

# Dependencies

## Server 
| Dependency     | Version   |
| -------------- | --------- |
| cors         | ^2.8.5  |
| dotenv       | ^16.0.0 |
| express      | ^4.17.3 |
| express-static | 1.2.6 |
| hstore       | ^0.0.1  |
| jsonwebtoken | ^8.5.1  |
| passport     | ^0.5.2  |
| passport-jwt | ^4.0.0  |
| path         | ^0.12.7 |
| pg           | ^8.7.3  |
| pgtools      | ^0.3.2  |
| psqlformat   | ^1.16.0 |

## Frontend 
| Dependency | Version |
| -------------- | --------- |
| @testing-library/jest-dom | ^5.16.4 |
| @testing-library/react | ^13.1.1 |
| @testing-library/user-event | ^13.5.0 |
| axios | ^0.27.2 |
| leaflet | ^1.8.0 |
| react | ^18.0.0 |
| react-dom | ^18.0.0 |
| react-icons | ^4.3.1 |
| react-leaflet | ^4.0.0 |
| react-router-dom | ^6.3.0 |
| react-scripts | 5.0.1 |
| web-vitals | ^2.1.4 |

# Running

The app can be viewed in two ways: localhost:5000 when the frontend is built and served as static or development server on localhost:3000

### The frontend served as static from the backend:

In ./frontend: npm i (install packages) AND npm run build (to build the frontend). <br>
In ./server: npm i (install packages) AND npm run dev (start server at localhost:5000) <br>
Currently the database needs to be created manually. The SQL for the tables and initial data is located in ./server/src/db/database.sql

### Development server:

In ./frontend: npm i (install packages) AND npm start <br>
In ./server: npm i (install packages) AND npm run dev <br>
Application can be viewed from localhost:3000 <br>
Currently the database needs to be created manually. The SQL for the tables and initial data is located in ./server/src/db/database.sql


You can either create your own user and log in or use one or more of the initial users to log in
<br>
minnie@example.com
<br>
pluto@example.com
<br>
donald@example.com
<br>
goofy@example.com
<br>
Password: Password1!

# Pictures

The front page shows the 5 most recent listings.
![Frontpage](/pictures/frontpage.png "Frontpage")
<br>
Clicking on a listing will take you to the listing page where you can read the details and see the approximate location where it can me picked up. It's also possible to make a bid and send a message to the seller
![listing](/pictures/listing.png "Listing page")
<br>
User can add new listings via a form.
![Adding a new listing](/pictures/newListing.png "Adding a new listing")
<br>
Users can find interesting listings by clicking through categories and subcategories in the sidebar
![Categories and subcategories](/pictures/category.png "Browsing the categories and subcategories")
<br>
User's conversations on different listings and users.
![Conversations](/pictures/messages.png "Conversations")
<br>
# Future development

This project is a continuation to a backend project in Buutti Trainee academy in the beginning of 2022. In the spring I started on the frontend with React. In the future hope to add a searchbar for finding listings more easily and also notifications so the user will notice if there are new messages. Also it would be nice to implement a location based seach once I have studied PostGIS a bit further. 

My aim is to dockerize the application and deploy it to heroku.

# Credits

Thank you to my instructors in Buutti Heli Isohätälä and Alku Mattila for your help.
